---
layout: markdown_page
title: "Data at GitLab"
---


## Problem Statement
    
The original charge of the GitLab Data Team was primarily focused on business metrics from our SaaS systems. Tools like Salesforce, Zuora, Zendesk, etc. have native reporting capabilities but some numbers (such as Gross / Net Retention) are only available after data integration.

Less than 10% of the data team's time was supporting Product-focused data sources such as our Usage and Version Ping data and GitLab.com database data. In August 2018, Snowplow event tracking was activated for Gitlab.com in a limited fashion.

Recently, the need for more product data is massively increasing. Several product managers are focused on data very intensely - namely the PMs of Fulfillment and Manage. Product wants more event data from Snowplow - we’re currently only tracking page views and page pings. It’s estimated that the volume of event data will increase by at least 5 to 10x. While there is 1 additional Growth Data Analysts to be onboarded, Product will still need to have their own analytic needs met as Growth analysts are specific to the Growth function. 

We’re also seeing an increase in the volume of requests coming to data from all groups and the desire for more teams to have their own Analyst as well. These teams also strongly desire deep data integration as well across all data sources (including product). 

We’ve seen an increase in requests in the following three categories:

* Debugging data quality issues across operational systems 
    * See https://gitlab.com/gitlab-data/analytics/issues/1179 as an example
* Building new data models
* Delivering new reports and analyses


## Summary of Past Efforts

The initial plan for the growth of the data team did not take into account this transition from Business Intelligence to BI + Product Analytics. Current data team head count is 1 Data Engineer, 4 Analysts, and 1 Manager supporting a company of 584 team members (1%). In surveying similar companies, and via conversations with the data community, we found data teams comprising anywhere from 2 to 8% of the company headcount. This would be a headcount of 12 to 48 for a company of 600 people and 24 to 98 for 1200 people.

From https://data36.com/data-team-structure-data-organization/ :

> Mid size companies (~500 employees) usually have at least 3-4 Data Engineers on the Data Infrastructure Team and around 6-10 Data Scientists and Analysts on the Analytics Team (sometimes Data Scientists and Analysts are split into 2 teams).  [This is 9-14 people or 2-3% for GitLab at current headcount]

We believe it’s also critical to work with the engineering infrastructure team. While there are currently issues in place to stand up our own infrastructure for collecting Snowplow event data, the nature of the problem space requires Data Engineers who will be concerned with the downstream process of the data. 


## Outline of options

* Reduce responsibility to not include Product Analytics

* Broaden the scope and responsibility of the data team
 
* Hire more people

* Utilize some infrastructure engineering resources. There are currently issues (https://gitlab.com/gitlab-data/analytics/issues/1140) in place to stand up our own infrastructure for collecting Snowplow event data, the nature of the problem space requires Data Engineers who will be concerned with the downstream process of the data. 

* Add a Director of Analytics/Data role who is solely focused on Data and KPIs across the organization

## Additional considerations

* The Manager of Data & Analytics will be an individual contributor on the Data Engineering side starting 2019-05-01
* One Data Analyst will transition to Data Engineer role soon
* An additional growth data analyst and finance analyst are planned hires

## Recommendations

* Aim for the data team to be 3% of headcount. 
* Formalize the data team into Reporting, Infrastructure, and Site Reliability Engineering (SRE) groups. The SRE group would work closely with the current Infrastructure Engineering team.