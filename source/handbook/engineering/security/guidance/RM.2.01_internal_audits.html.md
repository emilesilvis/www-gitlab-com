---
layout: markdown_page
title: "RM.2.01 - Internal Audits Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.2.01 - Internal Audits

## Control Statement

GitLab establishes internal audit requirements and executes audits on information systems and processes quarterly.

## Context

Audits are meant to validate processes and check to see if these controls we have implemented are having the desired effect and are performed the way we intended. Internal audits have a bad reputation, but these internal audits help the audit and compliance teams to build the information they need to be the main point of contact with external audits when needed. Successful internal audits can help keep external auditors away from Gitlabbers unless absolutely necessary.

## Scope

Internal audits are performed against all GitLab production systems and all processes that interact with those systems. Internal audits are also performed against all security compliance controls.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.2.01_internal_audits.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.2.01_internal_audits.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.2.01_internal_audits.md).

## Framework Mapping

* ISO
  * A.12.7.1
  * A.18.2.1
  * A.18.2.2
  * A.18.2.3
* SOC2 CC
  * CC1.2
  * CC3.2
  * CC3.4
  * CC4.1
  * CC4.2
  * CC5.1
  * CC5.2
